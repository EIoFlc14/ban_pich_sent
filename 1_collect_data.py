import snscrape.modules.twitter as sntwitter
import pymongo
import pandas as pd
maxTweets = 50000

# Conexion a MongoDB
client = pymongo.MongoClient("mongodb://EioFlc14:1rFuN3lVHYInggNR@cluster0-shard-00-00.84mxa.mongodb.net:27017,cluster0-shard-00-01.84mxa.mongodb.net:27017,cluster0-shard-00-02.84mxa.mongodb.net:27017/bp?ssl=true&replicaSet=atlas-ajl5mh-shard-0&authSource=admin&retryWrites=true&w=majority")
db = client["bp"]
collection = db["sent"]

cuenta = "@BancoPichincha"
since = 'since:2021-01-01'
until = 'until:2021-08-13'

tweets = []

for i,tweet in enumerate(sntwitter.TwitterSearchScraper(cuenta + ' ' + since + ' ' + until).get_items()):
    tweets.append({"Id_tweet":tweet.id,
                    "Created_At":tweet.date,
                    "Text":tweet.content,
                    "Author": '@'+tweet.username,
                    "Account_Mentioned":cuenta})

    print( str(i) + ' --- ' + str(tweet.date))

    if i == maxTweets:
        break

if len(tweets) > 0:
    collection.insert_many(tweets)
    df_tweets = pd.DataFrame(tweets, columns=['Id_tweet','Created_At','Text','Author','Account_Mentioned'])
    df_tweets.to_csv('tweets_banco_pic.csv',index=False)